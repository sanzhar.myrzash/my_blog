from typing import Any
from django.http import HttpRequest
from django.http.response import HttpResponse as HttpResponse
from django.shortcuts import render
from ..models import Author
from ..forms import AuthorForm
from django.views.generic import View

def author_create_view(request):
    if request.method == 'GET':
        form = AuthorForm()
        return render(request, 'authors/create.html', context={'form': form})
    elif request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = Author.objects.create(
                name=request.POST.get('name')
            )
            return render(request, 'authors/list.html')
        else:
            return render(request, 'authors/create.html', context={'form': form})
    
def author_list_view(request):
    return render(request, 'authors/list.html', context={
        'authors': Author.objects.all()
    })


class AuthorView(View):
    def get(self, request, *args: Any, **kwargs: Any):
        form = AuthorForm()
        return render(request, 'authors/create.html', context={'form': form})

    def post(self, request, *args: Any, **kwargs: Any):
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = Author.objects.create(
                name=request.POST.get('name')
            )
            return render(request, 'authors/list.html')
        else:
            return render(request, 'authors/create.html', context={'form': form})

    def dispatch(self, request, *args: Any, **kwargs: Any):
        if request.method == 'GET':
            return self.get(request, *args, **kwargs)
        elif request.method == 'POST':
            return self.post(request, *args, **kwargs)
        