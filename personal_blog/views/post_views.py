from django.shortcuts import render, redirect, get_object_or_404
# from django.http import Http404
from ..models import Post, Author
from ..forms import PostForm, CommentForm
from django.views.generic import View



# def index_view(request):
#     return render(request, 'index.html')

def post_list_view(request):
    posts = Post.objects.all()
    return render(request, 'posts/list.html', context={'posts': posts})

# def post_detail_view(request, *args, **kwargs):
#     # try:
#     #     post = Post.objects.get(pk=kwargs.get('pk'))
#     # except Post.DoesNotExist as error:
#     #     raise Http404
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))
#     return render(request, 'posts/detail_view.html', context={
#         'post': post
#     })

class PostDetailView(View):
    def get(self, request, *args, **kwargs):
        form = CommentForm()
        post = get_object_or_404(Post, pk=kwargs.get('pk'))
        return render(request, 'posts/detail.html', context={
            'post': post,
            'form': form,
        })
    
class CommentView(View):
    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)
        post_pk = kwargs.get('post_pk')
        if form.is_valid():
            post = get_object_or_404(Post, pk=post_pk)
            post.comments.create(
                text = request.POST.get('text'),
                author = request.POST.get('author')
            )
            return redirect('post_detail', post_pk)


def post_create_view(request):
    if request.method == 'GET':
        form = PostForm()
        return render(request, 'posts/create.html', context={
            'form': form,
            'authors': Author.objects.all(),
        })
    elif request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            author = Author.objects.get(pk=request.POST.get('author'))
            tags = form.cleaned_data.pop('tags')
            new_post = Post.objects.create(
                    title = form.cleaned_data['title'],
                    body = form.cleaned_data['body'],
                    author = author,
                )
            new_post.tags.set(tags)
            return redirect('post_list')
        else:
            return render(request, 'posts/create.html', context={
                'form': form})
        # return render(request, 'post_view.html', context={'post': new_post})

def post_update_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk = kwargs.get('pk'))
    if request.method == 'GET':
        form = PostForm(initial={
            'title': post.title,
            'body': post.body,
            'author': post.author,
            'tags': post.tags.all()
        })
        return render(request, 'posts/update.html', context={
            'form': form,
            'post': post
        })
    elif request.method == 'POST':
        form = PostForm(data=request.POST, initial={'tags': post.tags.all()})
        if form.is_valid():
            tags = form.cleaned_data.pop('tags')
            post.title = form.cleaned_data.get('title')
            post.body = form.cleaned_data.get('body')
            post.author = form.cleaned_data.get('author')
            post.save()
            post.tags.set(tags)
            return redirect('post_detail', pk=post.pk)
        else:
            return render(request, 'posts/update.html', context={
                'form': form,
                'post': post
            })

def post_delete_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'posts/delete.html', context={
            'post': post
        })
    elif request.method == 'POST':
        post.delete()
        return redirect('home_page')