from django.db import models

# Author Class
class Author(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name="Author's name")

    def __str__(self):
        return f"#{self.pk} - {self.name}"

# Post Class
class Post(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Title')
    body = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Body')
    author = models.ForeignKey(to=Author, on_delete=models.CASCADE, null=False, verbose_name='Author')
    # tags_old = models.ManyToManyField(
    #     'personal_blog.Tag',
    #     related_name = 'posts_old',
    #     through='personal_blog.PostTag',
    #     through_fields=('post', 'tag'),
    #     blank=True,
    # )
    tags = models.ManyToManyField(
        'personal_blog.Tag',
        related_name = 'posts',
        blank=True,
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Create Date")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Update Date")

    def __str__(self):
        return f"#{self.pk} - {self.title}"


# Comment Class
class Comment(models.Model):
    text = models.TextField(max_length=1000, null=False, blank=False, verbose_name="Comment")
    author = models.CharField(max_length=50, null=True, blank=True, default='Anonymous', verbose_name='Author')
    post = models.ForeignKey('personal_blog.Post', on_delete=models.CASCADE, related_name='comments', verbose_name='Post')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Create Date")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Update Date")

    def __str__(self):
        return self.text[:20]
    
# Class Tag
class Tag(models.Model):
    name = models.CharField(max_length=30, verbose_name='Tag')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Creat date")
    
    def __str__(self):
        return self.name
    
# # Intermediate class between Post and Tag 
# class PostTag(models.Model):
#     post = models.ForeignKey(
#         'personal_blog.Post', 
#         on_delete = models.CASCADE, 
#         related_name = 'post_tags', 
#         verbose_name='Post')
#     tag = models.ForeignKey(
#         'personal_blog.Tag',
#         on_delete = models.CASCADE,
#         related_name = 'tag_posts',
#         verbose_name = 'Tag'
#     )

#     def __str__(self):
#         return f"{self.post} | {self.tag}"
    
