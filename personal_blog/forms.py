from django import forms
from django.forms import widgets
from .models import Tag, Author

class PostForm(forms.Form):
    title = forms.CharField(
        max_length=200,
        required=True,
        label="Title*",
        widget = widgets.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Title',
        }))
    author = forms.ModelChoiceField(
        queryset=Author.objects.all(),
        required=True,
        label="Author*",
        widget=widgets.Select(attrs={
            'class': 'form-select',
            'placeholder': 'Author',
        }))
    body = forms.CharField(
        max_length=3000,
        required=False,
        label="Body",
        widget=widgets.Textarea(attrs={
            'class': 'form-control',
            'placeholder': 'Some text',
            'rows': 4
        }))
    tags = forms.ModelMultipleChoiceField(
        required = False,
        label = "Tags",
        queryset = Tag.objects.all(),
        widget=widgets.SelectMultiple(
            attrs={'class': 'form-select',})
        )

class AuthorForm(forms.Form):
    name = forms.CharField(
        max_length=200,
        required=True,
        label='Author',
        widget=widgets.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Author name',
                })
        )
    
class CommentForm(forms.Form):
    text = forms.CharField(
        max_length=1000, 
        required=True, 
        label="Comment",
        widget=widgets.Textarea(attrs={
            "class": "form-control"
        })
    )
    author = forms.CharField(
        max_length=50,
        required=False,
        label="Author",
        widget=widgets.TextInput(attrs={
            "class": "form-control",
            'placeholder': 'Author name',
        }),
        initial='Anonymous',
    )